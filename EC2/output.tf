output "NOME" {
  value = var.tags["Name"]
}

output "AMI" {
  value = var.ami
}

output "INSTANCE_TYPE" {
  value = var.instance_type
}

#output "IP_PUBLICO" {
#  value = aws_instance.terraform_instance.public_ip
#}

output "SUBNET" {
  value = var.subnet_id
}

output "SG_INSTANCIA" {
  value = var.security_group_ids
}

output "TAMANHO_VOLUME_ROOT" {
  value = var.root_volume_size
}

#output "TAMANHO_DISCO_ADICIONAL" {
#  value = var.volume_size_ebs[0]
#}

output "TIPO_VOLUME" {
  value = var.volume_type
}

output "IAM_ROLE" {
  value = aws_iam_role.ssm_role.arn
}