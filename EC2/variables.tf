variable "name_iam" {
  description = "Nome da role IAM"
  type        = string
}

variable "ami" {
  description = "ID da AMI"
  type        = string
}

variable "instance_type" {
  description = "Tipo de instância"
  type        = string
}

variable "key_name" {
  description = "Nome da chave SSH"
  type        = string
}

variable "subnet_id" {
  description = "ID da subnet"
  type        = string
}

variable "security_group_ids" {
  description = "Lista de IDs dos security groups"
  type        = list(string)
}

variable "public_ip" {
  description = "Indicador se o IP público deve ser associado"
  type        = bool
}

variable "monitoring" {
  description = "Indicador se o monitoramento detalhado deve ser habilitado"
  type        = bool
}

variable "disable_api_termination" {
  description = "Indicador se a instância não deve ser terminada via API"
  type        = bool
}

variable "volume_size" {
  description = "Tamanho do volume em GB"
  type        = number
}

variable "volume_type" {
  description = "Tipo de volume"
  type        = string
}

variable "encrypted" {
  description = "Indicador se o volume deve ser criptografado"
  type        = bool
}

variable "delete_on_termination" {
  description = "Indicador se o volume deve ser excluído na terminação"
  type        = bool
}

variable "user_data_windows" {
  description = "Script de inicialização do Windows"
  type        = string
}

variable "tags" {
  description = "Tags da instância"
  type        = map(string)
}
