resource "aws_instance" "instancia" {
  ami                         = var.ami
  instance_type               = var.instance_type
  key_name                    = var.key_name
  subnet_id                   = var.subnet_id
  vpc_security_group_ids      = var.security_group_ids
  associate_public_ip_address = var.public_ip
  monitoring                  = var.monitoring
  disable_api_termination     = var.disable_api_termination
  iam_instance_profile        = aws_iam_instance_profile.ssm_profile.name
  user_data                   = var.user_data_windows # Alterar Windows ou Linux conforme o SO
  tags                        = var.tags

  root_block_device {
    volume_size           = var.volume_size
    volume_type           = var.volume_type
    encrypted             = var.encrypted
    delete_on_termination = var.delete_on_termination
  }
}
